package biz.no_ip.palaven.weatherwatcher;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import nl.qbusict.cupboard.QueryResultIterable;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class Database {

    private DatabaseHelper mDatabaseHelper;

    public Database(Context context) {
        mDatabaseHelper = new DatabaseHelper(context);
    }

    public List<Weather> getWeatherList() {
        SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();
        List<Weather> weatherList = new ArrayList<>();
        try {
            // Iterate events
            QueryResultIterable<Weather> itr = cupboard().withDatabase(db).query(Weather.class).query();
            for (Weather weather : itr) {
                weatherList.add(weather);
            }
            itr.close();
        } catch (Exception e) {
            Log.e("Weather watcher", "Can't read weather from database");
        }

        return weatherList;
    }

    public long addWeather(Weather weather) {
        SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        return cupboard().withDatabase(db).put(weather);
    }

    public void updateWeather(Weather weather) {
        SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        cupboard().withDatabase(db).update(Weather.class, weatherToContentValues(weather));
        db.close();
    }

    public void removeWeather(Weather weather) {
        SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        cupboard().withDatabase(db).delete(Weather.class, weather.get_id());
        db.close();
    }

    public Weather getWeather(long id) {
        SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();
        Weather weather = cupboard().withDatabase(db).get(Weather.class, id);
        db.close();
        return weather;
    }

    private ContentValues weatherToContentValues(Weather weather) {
        return cupboard().withEntity(Weather.class).toContentValues(weather);
    }

}
