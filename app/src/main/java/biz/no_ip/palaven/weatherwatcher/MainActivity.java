package biz.no_ip.palaven.weatherwatcher;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity
        implements WeatherListFragment.OnListFragmentInteractionListener,
                    WeatherEditDialogFragment.OnSaveListener{

    private SwipeRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshLayout);
        refreshLayout.setOnRefreshListener(onRefreshListener);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            FragmentManager fragmentManager = getSupportFragmentManager();
            WeatherListFragment weatherListFragment =
                    (WeatherListFragment) fragmentManager.findFragmentById(R.id.fragment);
            weatherListFragment.refresh();
            refreshLayout.setRefreshing(false);
        }
    };

    @Override
    public void onListFragmentInteraction(Weather weather) {
        DialogFragment weatherFragment = new WeatherEditDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(WeatherEditDialogFragment.ARG_WEATHER, weather);
        weatherFragment.setArguments(args);
        weatherFragment.show(getSupportFragmentManager(), "weatherEdit");
    }

    @Override
    public void onSave(Weather weather) {
        Database db = new Database(this);
        db.updateWeather(weather);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:
                return true;
            case R.id.startWatcher:
                startWatcher();
                break;
            case R.id.stopWatcher:
                stopWatcher();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void stopWatcher() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),
                0, intent, 0);
        alarmManager.cancel(pendingIntent);
    }

    private void startWatcher() {
        Calendar startTime = Calendar.getInstance();
        startTime.set(Calendar.MINUTE,0);
        startTime.set(Calendar.SECOND,0);
        int hour = startTime.get(Calendar.HOUR_OF_DAY);
        if (hour>21) {
            startTime.add(Calendar.HOUR_OF_DAY, 3);
            startTime.set(Calendar.HOUR_OF_DAY, 1);
        } else if (hour>18) {
            startTime.set(Calendar.HOUR_OF_DAY, 22);
        } else if (hour>15) {
            startTime.set(Calendar.HOUR_OF_DAY, 19);
        } else if (hour>12) {
            startTime.set(Calendar.HOUR_OF_DAY, 16);
        } else if (hour>9) {
            startTime.set(Calendar.HOUR_OF_DAY, 13);
        } else if (hour>6) {
            startTime.set(Calendar.HOUR_OF_DAY, 10);
        } else if (hour>3) {
            startTime.set(Calendar.HOUR_OF_DAY, 7);
        } else if (hour>0) {
            startTime.set(Calendar.HOUR_OF_DAY, 4);
        } else {
            startTime.set(Calendar.HOUR_OF_DAY, 1);
        }

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),
                    0, intent, 0);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, startTime.getTimeInMillis(),
                3*60*60*1000, pendingIntent);

        Toast.makeText(this, "Alarm is set to "
                + DateFormat.format("dd/MM/yyyy HH:mm", startTime.getTimeInMillis()).toString(),
                Toast.LENGTH_LONG).show();
    }


}
