package biz.no_ip.palaven.weatherwatcher;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import biz.no_ip.palaven.weatherwatcher.WeatherListFragment.OnListFragmentInteractionListener;

import java.util.List;


public class WeatherRecyclerViewAdapter
        extends RecyclerView.Adapter<WeatherRecyclerViewAdapter.ViewHolder> {

    private final List<Weather> mValues;
    private final OnListFragmentInteractionListener mListener;

    public WeatherRecyclerViewAdapter(List<Weather> items,
                                      OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_weather, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.weather = mValues.get(position);
        holder.datetimeView.setText(mValues.get(position).getStrDatetime());
        holder.currentCloudsView.setText(Integer.toString(mValues.get(position).getCurrentClouds()));
        holder.forecastView.setText(Integer.toString(mValues.get(position).getForecastClouds()) + "\n"
                + Integer.toString(mValues.get(position).getForecastSunshine()));
        holder.realView.setText(Integer.toString(mValues.get(position).getRealClouds()) + "\n"
                + Integer.toString(mValues.get(position).getRealSunshine()));

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.weather);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final TextView datetimeView;
        public final TextView currentCloudsView;
        public final TextView forecastView;
        public final TextView realView;
        public Weather weather;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            datetimeView = (TextView) view.findViewById(R.id.datetime);
            currentCloudsView = (TextView) view.findViewById(R.id.currentClouds);
            forecastView = (TextView) view.findViewById(R.id.forecast);
            realView = (TextView) view.findViewById(R.id.real);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + datetimeView.getText() + "'";
        }
    }
}
