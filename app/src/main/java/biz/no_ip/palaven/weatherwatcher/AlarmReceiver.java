package biz.no_ip.palaven.weatherwatcher;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.text.format.DateFormat;

public class AlarmReceiver extends BroadcastReceiver {
    public AlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        showNotification(context);
        Intent serviceIntent = new Intent(context, WeatherWatcherService.class);
        context.startService(serviceIntent);
    }

    private void showNotification(Context context) {
        Intent myIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, myIntent, 0);

        NotificationManager nm = (NotificationManager) context.getSystemService(
                Context.NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setTicker("Выгляни в окно")
                .setContentTitle("Время определить погоду")
                .setContentText("Сервис запущен в " +
                        DateFormat.format("HH:mm", System.currentTimeMillis()).toString())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.ic_launcher))
                .setWhen(System.currentTimeMillis());
        Notification notification;
        //if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            notification = builder.build();
        /*} else {
            notification = builder.getNotification();
        }*/
        nm.notify(1, notification);
    }
}
