package biz.no_ip.palaven.weatherwatcher;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateFormat;

import java.util.Calendar;

public class Weather implements Parcelable{
    public Long _id;
    public long datetime;
    public int currentClouds;
    public int forecastClouds;
    public int forecastSunshine;
    public int realClouds;
    public int realSunshine;

    public Weather(Bundle args) {
        //this._id = args.getLong("_id",null);
        this.datetime = args.getLong("datetime", Calendar.getInstance().getTimeInMillis());
        this.currentClouds = args.getInt("currentClouds", 200);
        this.forecastClouds = args.getInt("forecastClouds", 200);
        this.forecastSunshine = args.getInt("forecastSunshine", 200);
        this.realClouds = args.getInt("realClouds", 200);
        this.realSunshine = args.getInt("realSunshine", 200);
    }

    public Weather() {
        //this._id = null;
        this.datetime = Calendar.getInstance().getTimeInMillis();
        this.currentClouds = 200;
        this.forecastClouds = 200;
        this.forecastSunshine = 200;
        this.realClouds = 200;
        this.realSunshine = 200;
    }

    public Weather(Parcel in) {
        String[] data = new String[6];
        in.readStringArray(data);
        _id = Long.parseLong(data[0]);
        datetime = Long.parseLong(data[1]);
        currentClouds = Integer.parseInt(data[2]);
        forecastClouds = Integer.parseInt(data[3]);
        forecastSunshine = Integer.parseInt(data[4]);
        realClouds = Integer.parseInt(data[5]);
        realSunshine = Integer.parseInt(data[6]);
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public long getDatetime() {
        return datetime;
    }

    public CharSequence getStrDatetime() {
        return DateFormat.format("dd/MM/yyyy HH:mm", datetime).toString();
    }

    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }

    public int getCurrentClouds() {
        return currentClouds;
    }

    public void setCurrentClouds(int currentClouds) {
        this.currentClouds = currentClouds;
    }

    public int getForecastClouds() {
        return forecastClouds;
    }

    public void setForecastClouds(int forecastClouds) {
        this.forecastClouds = forecastClouds;
    }

    public int getForecastSunshine() {
        return forecastSunshine;
    }

    public void setForecastSunshine(int forecastSunshine) {
        this.forecastSunshine = forecastSunshine;
    }

    public int getRealClouds() {
        return realClouds;
    }

    public void setRealClouds(int realClouds) {
        this.realClouds = realClouds;
    }

    public int getRealSunshine() {
        return realSunshine;
    }

    public void setRealSunshine(int realSunshine) {
        this.realSunshine = realSunshine;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{
                Long.toString(_id),
                Long.toString(datetime),
                Integer.toString(currentClouds),
                Integer.toString(forecastClouds),
                Integer.toString(forecastSunshine),
                Integer.toString(realClouds),
                Integer.toString(realSunshine)});
    }

    public static final Parcelable.Creator<Weather> CREATOR = new Parcelable.Creator<Weather>() {

        @Override
        public Weather createFromParcel(Parcel source) {
            return new Weather(source);
        }

        @Override
        public Weather[] newArray(int size) {
            return new Weather[size];
        }
    };
}
