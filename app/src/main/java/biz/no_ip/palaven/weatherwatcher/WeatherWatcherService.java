package biz.no_ip.palaven.weatherwatcher;

import android.app.Service;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.IBinder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

public class WeatherWatcherService extends Service {
    public WeatherWatcherService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        getWeather();
        return super.onStartCommand(intent, flags, startId);
    }

    private void getWeather() {
        ConnectivityManager myConnMgr = (ConnectivityManager) getSystemService(
                getApplicationContext().CONNECTIVITY_SERVICE);
        NetworkInfo networkinfo = myConnMgr.getActiveNetworkInfo();

        String weatherUrl = "http://api.worldweatheronline.com/free/v2/weather.ashx" +
                "?key=1abaf4a17e2a72f8f1c17ae13c369" +  //application key
                "&q=54.81,56.11" +                      //lat-lon Ufa
                "&num_of_days=1" +                      //only today
                "&tp=3" +                               //6-hour intervals
                "&format=json";

        if (networkinfo != null && networkinfo.isConnected()) {
            new GetWeatherTask().execute(weatherUrl);
        }
    }

    private class GetWeatherTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                return getOneWeather(urls[0]);
            } catch (IOException e) {
                e.printStackTrace();
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            parseWeather(result);
            super.onPostExecute(result);
        }
    }

    private int parseInt(String num) {
        try {
            return Integer.parseInt(num);
        } catch (Exception e) {
            return 100;
        }
    }

    private void parseWeather(String response) {
        try {
            JSONObject json = new JSONObject(response);
            JSONObject data = json.getJSONObject("data");
            JSONObject current_condition = data.getJSONArray("current_condition").getJSONObject(0);
            int current = parseInt(current_condition.getString("cloudcover"));

            Calendar datetime = Calendar.getInstance();
            int hour = datetime.get(Calendar.HOUR_OF_DAY);
            String hourStr = "";
            if ((hour>=0)&&(hour<=2)){
                hourStr = "100";
            } else if ((hour>=3)&&(hour<=5)){
                hourStr = "400";
            } else if ((hour>=6)&&(hour<=8)){
                hourStr = "700";
            } else if ((hour>=9)&&(hour<=11)){
                hourStr = "1000";
            } else if ((hour>=12)&&(hour<=14)){
                hourStr = "1300";
            } else if ((hour>=15)&&(hour<=17)){
                hourStr = "1600";
            } else if ((hour>=18)&&(hour<=20)){
                hourStr = "1900";
            } else if ((hour>=21)&&(hour<=23)){
                hourStr = "2200";
            }

            JSONObject weather = data.getJSONArray("weather").getJSONObject(0);
            JSONArray hourly = weather.getJSONArray("hourly");
            int i = 0;
            int forecast = 200;
            int sunshine = 200;
            do {
                String time = hourly.getJSONObject(i).getString("time");
                if (time.equals(hourStr)) {
                    forecast = parseInt(hourly.getJSONObject(i).getString("cloudcover"));
                    sunshine = parseInt(hourly.getJSONObject(i).getString("chanceofsunshine"));
                    break;
                }
                i++;
            } while (i < hourly.length());

            Weather result = new Weather();
            result.setDatetime(datetime.getTimeInMillis());
            result.setCurrentClouds(current);
            result.setForecastClouds(forecast);
            result.setForecastSunshine(sunshine);

            Database db = new Database(getApplicationContext());
            db.addWeather(result);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getOneWeather(String myurl) throws IOException {
            InputStream inputstream = null;
            String data = "";
            try {
                URL url = new URL(myurl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(100000);
                connection.setConnectTimeout(100000);
                connection.setRequestMethod("GET");
                connection.setInstanceFollowRedirects(true);
                connection.setUseCaches(false);
                connection.setDoInput(true);

                int responseCode = connection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) { // 200 OK
                    inputstream = connection.getInputStream();
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();

                    int read = 0;
                    while ((read = inputstream.read()) != -1) {
                        bos.write(read);
                    }
                    byte[] result = bos.toByteArray();
                    bos.close();

                    data = new String(result);

                }
                connection.disconnect();
                //return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (inputstream != null) {
                    inputstream.close();
                }
            }
            return data;
        }


}
