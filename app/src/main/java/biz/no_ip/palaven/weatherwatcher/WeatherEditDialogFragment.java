package biz.no_ip.palaven.weatherwatcher;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class WeatherEditDialogFragment extends DialogFragment {

    public static final String ARG_WEATHER = "weather";

    private Weather weather;

    private OnSaveListener mListener;

    private EditText cloudsView, sunshineView;

    public WeatherEditDialogFragment() {
        // Required empty public constructor
    }

    public static WeatherEditDialogFragment newInstance(Weather weather) {
        WeatherEditDialogFragment fragment = new WeatherEditDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_WEATHER, weather);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            weather = getArguments().getParcelable(ARG_WEATHER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weather_edit_dialog, container, false);
        Button btn = (Button) view.findViewById(R.id.button);
        btn.setOnClickListener(onClickListener);
        TextView datetimeView = (TextView) view.findViewById(R.id.datetimeView);
        datetimeView.setText(weather.getStrDatetime());
        cloudsView = (EditText) view.findViewById(R.id.cloudsView);
        cloudsView.setText(Integer.toString(weather.getRealClouds()));
        sunshineView = (EditText) view.findViewById(R.id.sunshineView);
        sunshineView.setText(Integer.toString(weather.getRealSunshine()));
        return view;
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            weather.setRealClouds(Integer.parseInt(cloudsView.getText().toString()));
            weather.setRealSunshine(Integer.parseInt(sunshineView.getText().toString()));
            mListener.onSave(weather);
            dismiss();
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSaveListener) {
            mListener = (OnSaveListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnSaveListener {
        void onSave(Weather weather);
    }
}
